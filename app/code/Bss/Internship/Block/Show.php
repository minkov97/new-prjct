<?php

namespace Bss\Internship\Block;

use Bss\Internship\Model\InternshipFactory;
use Magento\Framework\View\Element\Template\Context;
use Magento\Framework\App\Request\Http;

class Show extends \Magento\Framework\View\Element\Template
{
    /**
     * @var InternshipFactory
     */
    protected $_internship;

    /**
     * @var Http
     */
    protected $_request;

    /**
     * Index constructor.
     * @param Context $context
     * @param InternshipFactory $internship
     * @param Http $request
     */
    public function __construct(
        Context $context,
        InternshipFactory $internship,
        Http $request
    ) {
        $this->_internship = $internship;
        $this->_request = $request;
        parent::__construct($context);
    }

    /**
     * @return Internship
     */
    public function getInternshipById()
    {
        $id = $this->_request->getParam('id');
        return $this->_internship->create()->load($id);
    }
}
