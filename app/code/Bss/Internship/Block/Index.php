<?php

namespace Bss\Internship\Block;

use Bss\Internship\Model\ResourceModel\Internship\CollectionFactory;
use Magento\Framework\View\Element\Template\Context;

class Index extends \Magento\Framework\View\Element\Template
{
    /**
     * @var CollectionFactory
     */
    protected $_collectionFactory;

    /**
     * Index constructor.
     * @param Context $context
     * @param CollectionFactory $collectionFactory
     */
    public function __construct(
        Context $context,
        CollectionFactory $collectionFactory
    ) {
        $this->_collectionFactory = $collectionFactory;
        parent::__construct($context);
    }

    /**
     *
     * @return \Bss\Internship\Model\ResourceModel\Internship\Collection
     */
    public function getInternshipCollection()
    {
        return $this->_collectionFactory->create();
    }
}
