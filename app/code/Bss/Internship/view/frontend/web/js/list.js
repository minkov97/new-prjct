define([
    'jquery',
    'jquery/ui',
    'owlCarousel'
], function ($) {
    'use strict';

    $.widget('bss.internshipList', {
        options: {},

        /**
         * @private
         */
        _create: function () {
            console.log(this.element);
            this.element.owlCarousel(this.options);
        },
    });

    return $.bss.internshipList;
});
