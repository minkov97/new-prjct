define([
    'jquery'
], function ($) {
    'use strict'
    return function(config, element) {
        $(document).on('ajax:addToCart', function (){
            $.ajax({
                url: config.url,
                method: "POST",
                data: {},
                dataType: "json",
                success: function (data)
                {
                    $(element).html("Subtotal: " + data['subtotal']);
                    var popup = $(element)
                        .modal({
                            modalClass: 'add-to-cart-popup',
                            title: $.mage.__("Pop up"),
                            buttons: [
                                {
                                    text: 'Continue Shopping',
                                    click: function () {
                                        this.closeModal();
                                    }
                                }
                            ]
                        });
                    popup.modal('openModal');
                }
            });
        })
    }
});