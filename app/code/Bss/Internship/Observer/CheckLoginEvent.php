<?php
namespace Bss\Internship\Observer;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\UrlInterface;

class CheckLoginEvent implements ObserverInterface
{
    /**
     * @var UrlInterface
     */
    protected $_url;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var ManagerInterface
     */
    protected $_messageManager;

    /**
     * CheckLoginEvent constructor.
     * @param UrlInterface $url
     * @param Session $session
     * @param ManagerInterface $messageManager
     */
    public function __construct(
        UrlInterface $url,
        Session $session,
        ManagerInterface $messageManager
    ) {
        $this->session = $session;
        $this->_url = $url;
        $this->_messageManager = $messageManager;
    }

    /**
     * @param Observer $observer
     */
    public function execute(Observer $observer)
    {
        if ($this->session->isLoggedIn() == false) {
            $this->_messageManager->addErrorMessage(__('You have to login'));
            $customerBeforeAuthUrl = $this->_url->getUrl('customer/account/login');
            $observer->getControllerAction()->getResponse()->setRedirect($customerBeforeAuthUrl);
        }
    }
}
