<?php
namespace Bss\Internship\Observer;

use Magento\Checkout\Model\Cart;
use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Message\ManagerInterface;
use Magento\InventorySalesApi\Api\GetProductSalableQtyInterface;
use Magento\InventorySalesApi\Api\StockResolverInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\InventorySalesApi\Api\Data\SalesChannelInterface;

class GetQtyAfterAddToCart implements ObserverInterface
{
    /**
     * @var ManagerInterface
     */
    protected $_messageManager;

    /**
     * @var Cart
     */
    protected $cart;

    /**
     * @var GetProductSalableQtyInterface
     */
    private $productSalableQty;

    /**
     * @var StockResolverInterface
     */
    private $stockResolver;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * GetQtyAfterAddToCart constructor.
     * @param ManagerInterface $messageManager
     * @param Cart $cart
     * @param GetProductSalableQtyInterface $productSalableQty
     * @param StockResolverInterface $stockResolver
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ManagerInterface $messageManager,
        Cart $cart,
        GetProductSalableQtyInterface $productSalableQty,
        StockResolverInterface $stockResolver,
        StoreManagerInterface $storeManager
    ) {
        $this->_messageManager = $messageManager;
        $this->cart = $cart;
        $this->productSalableQty = $productSalableQty;
        $this->stockResolver = $stockResolver;
        $this->storeManager = $storeManager;
    }

    /**
     * @param Observer $observer
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function execute(Observer $observer)
    {
        $product = $observer->getData('product');
        $websiteCode = $this->storeManager->getWebsite()->getCode();
        $stock = $this->stockResolver->execute(SalesChannelInterface::TYPE_WEBSITE, $websiteCode);
        $salableQty = $this->productSalableQty->execute($product->getSku(), $stock->getStockId());
        $cartProductQty = $this->cart->getQuote()->getItemByProduct($product)->getQty();
        $this->_messageManager->addSuccessMessage(__("Available products: ") . ($salableQty - $cartProductQty));
    }
}
