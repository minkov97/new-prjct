<?php
namespace Bss\Internship\Model\ResourceModel;

class Internship extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init('internship', 'id');
    }
}
