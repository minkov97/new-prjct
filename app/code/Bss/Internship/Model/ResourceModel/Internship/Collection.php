<?php
namespace Bss\Internship\Model\ResourceModel\Internship;

class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    /**
     * @var string
     */
    protected $_idFieldName = 'id';

    /**
     * Define resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init(
            \Bss\Internship\Model\Internship::class,
            \Bss\Internship\Model\ResourceModel\Internship::class
        );
    }
}
