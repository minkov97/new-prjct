<?php
namespace Bss\Internship\Model;

use Bss\Internship\Api\Data\date;
use Bss\Internship\Api\Data\InternshipInterface;
use Bss\Internship\Api\Data\InternshipSearchResultInterface;
use Magento\Framework\Model\AbstractExtensibleModel;

class Internship extends AbstractExtensibleModel implements InternshipInterface
{
    const NAME = 'name';
    const AVATAR = 'avatar';
    const DOB = 'dob';
    const DESCRIPTION = 'description';

    /**
     * Initialize resource model.
     */
    protected function _construct()
    {
        $this->_init(\Bss\Internship\Model\ResourceModel\Internship::class);
    }

    /**
     * @return mixed|string|null
     */
    public function getName()
    {
        return $this->_getData(self::NAME);
    }

    /**
     * @param $name
     */
    public function setName($name)
    {
        $this->setData(self::NAME, $name);
    }

    /**
     * @return mixed|string|null
     */
    public function getAvatar()
    {
        return $this->_getData(self::AVATAR);
    }

    /**
     * @param $avatar
     */
    public function setAvatar($avatar)
    {
        $this->setData(self::AVATAR, $avatar);
    }

    /**
     * @return date|mixed|null
     */
    public function getDob()
    {
        return $this->_getData(self::DOB);
    }

    /**
     * @param $dob
     */
    public function setDob($dob)
    {
        $this->setData(self::DOB, $dob);
    }

    /**
     * @return mixed|string|null
     */
    public function getDescription()
    {
        return $this->_getData(self::DESCRIPTION);
    }

    /**
     * @param $description
     */
    public function setDescription($description)
    {
        $this->setData(self::DESCRIPTION, $description);
    }
}
