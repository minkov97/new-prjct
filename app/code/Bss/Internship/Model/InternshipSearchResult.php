<?php

namespace Bss\Internship\Model;

use Magento\Framework\Api\SearchResults;
use Bss\Internship\Api\Data\InternshipSearchResultInterface;

class InternshipSearchResult extends SearchResults implements InternshipSearchResultInterface
{
}
