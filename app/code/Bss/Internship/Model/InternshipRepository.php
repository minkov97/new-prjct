<?php
namespace Bss\Internship\Model;

use Bss\Internship\Api\InternshipRepositoryInterface;
use Bss\Internship\Api\Data\InternshipInterface;
use Bss\Internship\Api\Data\InternshipSearchResultInterfaceFactory;
use Bss\Internship\Model\ResourceModel\Internship\CollectionFactory as InternshipCollectionFactory;
use Magento\Framework\Api\SearchCriteria\CollectionProcessorInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\NoSuchEntityException;

class InternshipRepository implements InternshipRepositoryInterface
{
    /**
     * @var InternshipFactory
     */
    private $internshipFactory;

    /**
     * @var InternshipCollectionFactory
     */
    private $internshipCollectionFactory;

    /**
     * @var InternshipSearchResultInterfaceFactory
     */
    private $searchResultFactory;

    /**
     * @var CollectionProcessorInterface
     */
    private $collectionProcessor;

    /**
     * InternshipRepository constructor.
     * @param InternshipFactory $internshipFactory
     * @param InternshipCollectionFactory $internshipCollectionFactory
     * @param InternshipSearchResultInterfaceFactory $searchResultFactory
     * @param CollectionProcessorInterface $collectionProcessor
     */
    public function __construct(
        InternshipFactory $internshipFactory,
        InternshipCollectionFactory $internshipCollectionFactory,
        InternshipSearchResultInterfaceFactory $searchResultFactory,
        CollectionProcessorInterface $collectionProcessor
    ) {
        $this->internshipFactory = $internshipFactory;
        $this->internshipCollectionFactory = $internshipCollectionFactory;
        $this->searchResultFactory = $searchResultFactory;
        $this->collectionProcessor = $collectionProcessor;
    }

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return mixed
     */
    public function getList(SearchCriteriaInterface $searchCriteria)
    {
        $collection = $this->internshipCollectionFactory->create();
        $this->collectionProcessor->process($searchCriteria, $collection);
        $searchResults = $this->searchResultFactory->create();

        $searchResults->setSearchCriteria($searchCriteria);
        $searchResults->setItems($collection->getItems());

        return $searchResults;
    }

    /**
     * @param int $id
     * @return InternshipInterface|Internship
     * @throws NoSuchEntityException
     */
    public function getById(int $id)
    {
        $internship = $this->internshipFactory->create();
        $internship->getResource()->load($internship, $id);
        if (! $internship->getId()) {
            throw new NoSuchEntityException(__('Unable to find internship with ID "%1"', $id));
        }

        return $internship;
    }

    /**
     * @param InternshipInterface $internship
     * @return InternshipInterface
     */
    public function save(InternshipInterface $internship)
    {
        $internship->getResource()->save($internship);
        return $internship;
    }

    /**
     * @param InternshipInterface $internship
     */
    public function delete(InternshipInterface $internship)
    {
        $internship->getResource()->delete($internship);
    }
}
