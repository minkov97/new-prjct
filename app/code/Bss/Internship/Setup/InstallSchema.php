<?php
namespace Bss\Internship\Setup;

use Magento\Framework\DB\Ddl\Table;
use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

class InstallSchema implements InstallSchemaInterface
{
    /**
     * Create table internship.
     * @param SchemaSetupInterface $setup
     * @param ModuleContextInterface $context
     * @throws \Zend_Db_Exception
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $table = $setup->getConnection()
            ->newTable($setup->getTable('internship'))
            ->addColumn(
                'id',
                Table::TYPE_INTEGER,
                null,
                ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
                'ID'
            )->addColumn(
                'name',
                Table::TYPE_TEXT,
                255,
                ['nullable' => false, 'default' => ''],
                'Name'
            )->addColumn(
                'avatar',
                Table::TYPE_TEXT,
                255,
                ['nullable' => false, 'default' => ''],
                'Avatar'
            )->addColumn(
                'dob',
                Table::TYPE_DATE,
                null,
                ['nullable' => true, 'default' => '1991-1-1'],
                'DOB'
            )->addColumn(
                'description',
                Table::TYPE_TEXT,
                255,
                ['nullable' => false, 'default' => ''],
                'Description'
            )->setComment("Internship table");
        $setup->getConnection()->createTable($table);
    }
}
