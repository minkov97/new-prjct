<?php
namespace Bss\Internship\Setup;

use Magento\Framework\Setup\InstallDataInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\ModuleDataSetupInterface;

class InstallData implements InstallDataInterface
{
    /**
     * Insert data into table internship.
     * @param ModuleDataSetupInterface $setup
     * @param ModuleContextInterface $context
     */
    public function install(ModuleDataSetupInterface $setup, ModuleContextInterface $context)
    {
        $data = [
            [
                'name' => 'Example A',
                'avatar' => 'https://i.pinimg.com/originals/0d/41/3f/0d413f71e5499ecd15f5e84cf7bdf1ee.jpg',
                'dob' => '1991-1-1',
                'description' => 'nothing'
            ],
            [
                'name' => 'Example B',
                'avatar' => 'https://i.pinimg.com/originals/0d/41/3f/0d413f71e5499ecd15f5e84cf7bdf1ee.jpg',
                'dob' => '1991-1-2',
                'description' => 'nothing'
            ],
            [
                'name' => 'Example C',
                'avatar' => 'https://i.pinimg.com/originals/0d/41/3f/0d413f71e5499ecd15f5e84cf7bdf1ee.jpg',
                'dob' => '1991-1-3',
                'description' => 'nothing'
            ],
            [
                'name' => 'Example D',
                'avatar' => 'https://i.pinimg.com/originals/0d/41/3f/0d413f71e5499ecd15f5e84cf7bdf1ee.jpg',
                'dob' => '1991-1-4',
                'description' => 'nothing'
            ],
            [
                'name' => 'Example E',
                'avatar' => 'https://i.pinimg.com/originals/0d/41/3f/0d413f71e5499ecd15f5e84cf7bdf1ee.jpg',
                'dob' => '1991-1-5',
                'description' => 'nothing'
            ]
        ];
        foreach ($data as $bind) {
            $setup->getConnection()
                ->insertForce($setup->getTable('internship'), $bind);
        }
    }
}
