<?php
namespace Bss\Internship\Controller\Add;

use Magento\Framework\App\Action\Context;
use Magento\Framework\App\Action\HttpPostActionInterface;
use Magento\Framework\View\Result\PageFactory;
use Bss\Internship\Model\InternshipFactory;

class Add extends \Magento\Framework\App\Action\Action implements HttpPostActionInterface
{
    /**
     * @var PageFactory
     */
    protected $_pageFactory;

    /**
     * @var InternshipFactory
     */
    protected $_internshipFactory;

    /**
     * Index constructor.Bss\Internship\Model
     * @param Context $context
     * @param InternshipFactory $internshipFactory
     */
    public function __construct(
        Context $context,
        InternshipFactory $internshipFactory
    ) {
        $this->_internshipFactory = $internshipFactory;
        parent::__construct($context);
    }

    /**
     *
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $request = $this->getRequest();
        $internship = $this->_internshipFactory->create();

        $data = [
            'name' => $request->getParam('name'),
            'avatar' => $request->getParam('avatar'),
            'dob' => $request->getParam('dob'),
            'description' => $request->getParam('description')
        ];
        try {
            $internship->setData($data);
            $internship->save();
            $this->messageManager->addSuccessMessage(__('Success'));
        } catch (\Exception $e) {
            $this->messageManager->addErrorMessage(__('Something went wrong while saving your internship.'));
        }
        return $this->_redirect('internship/add/index');
    }
}
