<?php
namespace Bss\Internship\Plugin;

use Magento\Framework\Message\MessageInterface;
use Magento\Customer\Model\Session;
use Magento\Framework\Message\ManagerInterface;

class MessagePlugin
{
    /**
     * @var Session
     */
    protected $session;

    /**
     * MessagePlugin constructor.
     * @param Session $session
     */
    public function __construct(Session $session)
    {
        $this->session = $session;
    }

    /**
     * @param ManagerInterface $subject
     * @param MessageInterface $text
     * @return MessageInterface[]
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function beforeAddMessage(ManagerInterface $subject, MessageInterface $text, $group = null)
    {
        $msg = $text->getText();
        if ($this->session->isLoggedIn() == false) {
            $msg = __("Dear guest, ") . $msg;
        } else {
            $msg = __("Dear ") . $this->session->getCustomer()->getName() . ", " . $msg;
        }

        $text->setText($msg);
        return [$text, $group];
    }
}
