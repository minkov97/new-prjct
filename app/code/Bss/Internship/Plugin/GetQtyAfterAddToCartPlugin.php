<?php
namespace Bss\Internship\Plugin;

use Magento\Checkout\Model\Cart;
use Magento\Framework\Message\ManagerInterface;
use Magento\InventorySalesAdminUi\Model\GetSalableQuantityDataBySku;
use Magento\Catalog\Model\Product;
use Magento\InventorySalesApi\Api\GetProductSalableQtyInterface;
use Magento\InventorySalesApi\Api\StockResolverInterface;
use Magento\Store\Model\StoreManagerInterface;
use Magento\InventorySalesApi\Api\Data\SalesChannelInterface;

class GetQtyAfterAddToCartPlugin
{
    /**
     * @var GetSalableQuantityDataBySku
     */
    protected $salableQuantityDataBySku;

    /**
     * @var ManagerInterface
     */
    protected $_messageManager;

    /**
     * @var GetProductSalableQtyInterface
     */
    private $productSalableQty;

    /**
     * @var StockResolverInterface
     */
    private $stockResolver;

    /**
     * @var StoreManagerInterface
     */
    private $storeManager;

    /**
     * GetQtyAfterAddToCartPlugin constructor.
     * @param ManagerInterface $messageManager
     * @param GetSalableQuantityDataBySku $salableQuantityDataBySku
     * @param GetProductSalableQtyInterface $productSalableQty
     * @param StockResolverInterface $stockResolver
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        ManagerInterface $messageManager,
        GetSalableQuantityDataBySku $salableQuantityDataBySku,
        GetProductSalableQtyInterface $productSalableQty,
        StockResolverInterface $stockResolver,
        StoreManagerInterface $storeManager
    ) {
        $this->_messageManager = $messageManager;
        $this->salableQuantityDataBySku = $salableQuantityDataBySku;
        $this->productSalableQty = $productSalableQty;
        $this->stockResolver = $stockResolver;
        $this->storeManager = $storeManager;
    }

    /**
     * @param Cart $subject
     * @param Cart $result
     * @param \Magento\Framework\DataObject|int|array $productInfo
     * @return Cart
     * @throws \Magento\Framework\Exception\InputException
     * @throws \Magento\Framework\Exception\LocalizedException
     * @throws \Magento\Framework\Exception\NoSuchEntityException
     */
    public function afterAddProduct(Cart $subject, Cart $result, $productInfo)
    {
        if ($productInfo instanceof Product) {
            $websiteCode = $this->storeManager->getWebsite()->getCode();
            $stock = $this->stockResolver->execute(SalesChannelInterface::TYPE_WEBSITE, $websiteCode);
            $salableQty = $this->productSalableQty->execute($productInfo->getSku(), $stock->getStockId());
            $cartProductQty = $subject->getQuote()->getItemByProduct($productInfo)->getQty();
            $this->_messageManager->addSuccessMessage(__("Available products: ") . ($salableQty - $cartProductQty));
        }
        return $result;
    }
}
