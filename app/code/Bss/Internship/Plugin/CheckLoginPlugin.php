<?php
namespace Bss\Internship\Plugin;

use Magento\Customer\Model\Session;
use Magento\Framework\App\Response\Http;
use Magento\Framework\Message\ManagerInterface;
use Magento\Framework\UrlInterface;

class CheckLoginPlugin
{
    /**
     * @var UrlInterface
     */
    protected $_url;

    /**
     * @var Session
     */
    protected $session;

    /**
     * @var ManagerInterface
     */
    protected $_messageManager;

    /**
     * @var Http
     */
    protected $response;

    /**
     * CheckLoginPlugin constructor.
     * @param UrlInterface $url
     * @param Session $session
     * @param ManagerInterface $messageManager
     * @param Http $response
     */
    public function __construct(
        UrlInterface $url,
        Session $session,
        ManagerInterface $messageManager,
        Http $response
    ) {
        $this->session = $session;
        $this->_url = $url;
        $this->_messageManager = $messageManager;
        $this->response = $response;
    }

    /**
     * @param \Magento\Catalog\Controller\Product\View $subject
     */
    public function beforeExecute(\Magento\Catalog\Controller\Product\View $subject)
    {
        if ($this->session->isLoggedIn() == false) {
            $this->_messageManager->addErrorMessage(__('You have to login (msg plugin)'));
            $url = $this->_url->getUrl('customer/account/login');
            $this->response->setRedirect($url);
        }
    }
}
