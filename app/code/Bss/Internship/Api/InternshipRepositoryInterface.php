<?php
namespace Bss\Internship\Api;

use Bss\Internship\Api\Data\InternshipInterface;
use Bss\Internship\Api\Data\InternshipSearchResultInterface;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\NoSuchEntityException;

interface InternshipRepositoryInterface
{
    /**
     * @param int $id
     * @return InternshipInterface
     * @throws NoSuchEntityException
     */
    public function getById(int $id);

    /**
     * @param InternshipInterface $internship
     * @return InternshipInterface
     */
    public function save(InternshipInterface $internship);

    /**
     * @param InternshipInterface $internship
     * @return void
     */
    public function delete(InternshipInterface $internship);

    /**
     * @param SearchCriteriaInterface $searchCriteria
     * @return InternshipSearchResultInterface
     */
    public function getList(SearchCriteriaInterface $searchCriteria);
}
