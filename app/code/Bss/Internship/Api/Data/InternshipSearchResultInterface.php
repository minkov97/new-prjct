<?php
namespace Bss\Internship\Api\Data;

use Magento\Framework\Api\SearchResultsInterface;

interface InternshipSearchResultInterface extends SearchResultsInterface
{
    /**
     * @return Bss\Internship\Api\Data\InternshipInterface[]
     */
    public function getItems();

    /**
     * @param Bss\Internship\Api\Data\InternshipInterface[] $item
     * @return void
     */
    public function setItems(array $item);
}
