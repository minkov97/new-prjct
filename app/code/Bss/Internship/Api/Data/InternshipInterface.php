<?php
namespace Bss\Internship\Api\Data;

use Magento\Framework\Api\ExtensibleDataInterface;

interface InternshipInterface extends ExtensibleDataInterface
{
    /**
     * @return int
     */
    public function getId();

    /**
     * @param $id
     * @return void
     */
    public function setId($id);

    /**
     * @return string
     */
    public function getName();

    /**
     * @param $name
     * @return void
     */
    public function setName($name);

    /**
     * @return string
     */
    public function getAvatar();

    /**
     * @param $avatar
     * @return void
     */
    public function setAvatar($avatar);

    /**
     * @return date
     */
    public function getDob();

    /**
     * @param $dob
     * @return void
     */
    public function setDob($dob);

    /**
     * @return string
     */
    public function getDescription();

    /**
     * @param $description
     * @return void
     */
    public function setDescription($description);
}
