<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Bss\HelloWorld\Model\Config\Source;

class Age implements \Magento\Framework\Option\ArrayInterface
{
    /**
     * @return array
     */
    public function toOptionArray()
    {
        $returnValue = [];
        for ($age = 1; $age < 100; $age++) {
            $returnValue[] = ['value' => $age, 'label' => $age];
        }
        return $returnValue;
    }
}
