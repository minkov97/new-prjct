<?php

namespace Bss\HelloWorld\Controller\Index;

use Magento\Framework\App\Action\Context;

class Frwrd extends \Magento\Framework\App\Action\Action
{
    /**
     *
     * @return void
     */
    public function execute()
    {
        $this->_forward('index', 'index', 'cms');
    }
}
