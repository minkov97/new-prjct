<?php

namespace Bss\HelloWorld\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\JsonFactory;

class ResultJson extends \Magento\Framework\App\Action\Action
{
    /**
     * @var JsonFactory
     */
    private $jsonResultFactory;

    /**
     * ResultJson constructor.
     * @param Context $context
     * @param JsonFactory $jsonResultFactory
     */
    public function __construct(
        Context $context,
        JsonFactory $jsonResultFactory
    ) {
        $this->jsonResultFactory = $jsonResultFactory;
        parent::__construct($context);
    }

    /**
     *
     * @return \Magento\Framework\Controller\Result\Json
     */
    public function execute()
    {
        $data = ['message' => 'Success'];
        $result = $this->jsonResultFactory->create();
        $result->setData($data);
        return $result;
    }
}
