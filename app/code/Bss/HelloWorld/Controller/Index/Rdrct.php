<?php

namespace Bss\HelloWorld\Controller\Index;

use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\Result\RedirectFactory;

class Rdrct extends \Magento\Framework\App\Action\Action
{
    /**
     * @var RedirectFactory
     */
    protected $redirectResultFactory;

    /**
     * Rdrct constructor.
     * @param Context $context
     * @param RedirectFactory $redirectResultFactory
     */
    public function __construct(
        Context $context,
        \Magento\Framework\Controller\Result\RedirectFactory $redirectResultFactory
    ) {
        $this->redirectResultFactory = $redirectResultFactory;
        parent::__construct($context);
    }

    /**
     *
     * @return \Magento\Framework\Controller\Result\Redirect
     */
    public function execute()
    {
        $result = $this->redirectResultFactory->create();
        $result->setPath('home');
        return $result;
    }
}
