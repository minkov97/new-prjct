<?php
namespace Bss\HelloWorld\Controller\Sales;

use Magento\Framework\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;

class Page extends \Magento\Framework\App\Action\Action
{
    protected $_pageFactory;

    public function __construct(
        PageFactory $pageFactory,
        Context $context
    ) {
        $this->_pageFactory = $pageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        return $this->_pageFactory->create();
    }
}