<?php
namespace Bss\HelloWorld\Block;

use Magento\Cms\Model\BlockFactory;
use Magento\Framework\View\Element\Template;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Cms\Model\Template\FilterProvider;

class Footer extends Template
{
    /**
     * @var BlockFactory
     */
    protected $_blockFactory;

    /**
     * @var StoreManagerInterface
     */
    protected $_storeManager;

    /**
     * @var FilterProvider
     */
    protected $_filterProvider;

    /**
     * Footer constructor.
     * @param Template\Context $context
     * @param BlockFactory $blockFactory
     * @param StoreManagerInterface $storeManager
     * @param FilterProvider $filterProvider
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        BlockFactory $blockFactory,
        StoreManagerInterface  $storeManager,
        FilterProvider $filterProvider,
        array $data = []
    ) {
        $this->_blockFactory = $blockFactory;
        $this->_storeManager = $storeManager;
        $this->_filterProvider = $filterProvider;
        parent::__construct($context, $data);
    }

    /**
     *
     * @return string
     */
    public function getStaticBlockContent()
    {
        return __('Hellooooooooooooo');
    }
}
