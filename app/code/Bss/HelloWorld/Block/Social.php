<?php
namespace Bss\HelloWorld\Block;

use Magento\Framework\View\Element\Template;

class Social extends Template
{
    /**
     *
     * @return \string[][]
     */
    public function getSocials()
    {
        return [
            ['img' => 'pub/media/training/images/facebook.png', 'title' => 'Facebook', 'path' => 'home'],
            ['img' => 'pub/media/training/images/twitter.png', 'title' => 'Twitter', 'path' => 'home'],
            ['img' => 'pub/media/training/images/instagram.png', 'title' => 'Instagram', 'path' => 'home'],
            ['img' => 'pub/media/training/images/linkedin.png', 'title' => 'Linkedin', 'path' => 'home']
        ];
    }
}
