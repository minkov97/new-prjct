<?php
namespace Bss\HelloWorld\Block;

use Magento\Framework\View\Element\Template;

class Link extends Template
{
    /**
     *
     * @return \string[][]
     */
    public function getLinks()
    {
        return [
            ['name' => 'home', 'title' => 'Home', 'path' => 'home'],
            ['name' => 'matches', 'title' => 'Matches', 'path' => 'home'],
            ['name' => 'news', 'title' => 'News', 'path' => 'home'],
            ['name' => 'team', 'title' => 'Team', 'path' => 'home'],
            ['name' => 'aboutus', 'title' => 'About Us', 'path' => 'home'],
            ['name' => 'privacypolicy', 'title' => 'Privacy Policy', 'path' => 'home'],
            ['name' => 'contactus', 'title' => 'Contact Us', 'path' => 'home'],
            ['name' => 'membership', 'title' => 'Membership', 'path' => 'home']
        ];
    }
}
