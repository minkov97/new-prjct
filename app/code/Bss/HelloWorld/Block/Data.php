<?php
namespace Bss\HelloWorld\Block;

use Magento\Catalog\Model\CategoryFactory as Category;
use Magento\Framework\Registry;
use Magento\Framework\View\Element\Template;

class Data extends Template
{
    /**
     * @var Category
     */
    protected $_category;

    /**
     * @var Registry
     */
    protected $_registry;

    /**
     * Data constructor.
     * @param Template\Context $context
     * @param Category $category
     * @param Registry $registry
     * @param array $data
     */
    public function __construct(
        Template\Context $context,
        Category $category,
        Registry $registry,
        array $data = []
    ) {
        $this->_category = $category;
        $this->_registry = $registry;
        parent::__construct($context, $data);
    }

    /**
     * @return array
     */
    public function getCategoryData()
    {
        $data = $this->_registry->registry('current_category');
        if(!isset($data)) return [];
        $category = $this->_category->create()->load($data->getId());
        return $category->getProductCollection();
    }
}
