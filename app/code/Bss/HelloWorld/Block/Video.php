<?php
namespace Bss\HelloWorld\Block;

use Magento\Framework\View\Element\Template;

class Video extends Template
{
    /**
     *
     * @return string
     */
    public function getVideo()
    {
        return __("https://www.youtube.com/embed/YiiHfOAG2Hk");
    }
}
